#pragma once

#include <Engine\Window.h>
#include <Engine\GLSLProgram.h>
#include <Engine\Camera2D.h>
#include <Engine\InputManager.h>
#include <Engine\SpriteBatch.h>

#include "Player.h"
#include "Level.h"

class Zombie;

enum class GameState {PLAY, EXIT};

class MainGame
{
public:
	MainGame();
	~MainGame();

	//Runs the game
	void run();

private:
	//Initializes the core systems
	void initSystems();
	//Initializes the level
	void initLevel();
	//Initializes the shaders
	void initShaders();
	//Main game loop for the game
	void gameLoop();
	//Updates all agents
	void updateAgents();
	//Handles input processing
	void processInput();
	//Renders the game
	void drawGame();

	Engine::Window _window; //The game window
	Engine::GLSLProgram _textureProgram; //The shader program
	Engine::InputManager _inputManager; //Handles Input
	Engine::Camera2D _camera; //Main camera
	Engine::SpriteBatch _agentSpriteBatch;

	std::vector<Level*> _levels;

	int _screenWidth, _screenHeight;

	float _fps;

	int _currentLevel;

	Player* _player;
	std::vector<Human*> _humans;
	std::vector<Zombie*> _zombies;

	//GameState
	GameState _gameState;
};